﻿#include <iostream>
using namespace std;

int main()
{
    unsigned int x;
    bool LeapYear;
    cout << "Enter a year: ";
    cin >> x;
    if (x % 4 != 0)
        LeapYear = false;
    else if (x % 400 != 0 and x % 100 == 0)
        LeapYear = false;
    else
        LeapYear = true;
    
    if (LeapYear)
        cout << "Year is leap.";
    else
        cout << "Year is not leap.";
}